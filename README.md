# NEEventCalendar

[![CI Status](https://img.shields.io/travis/1217493217@qq.com/NEEventCalendar.svg?style=flat)](https://travis-ci.org/1217493217@qq.com/NEEventCalendar)
[![Version](https://img.shields.io/cocoapods/v/NEEventCalendar.svg?style=flat)](https://cocoapods.org/pods/NEEventCalendar)
[![License](https://img.shields.io/cocoapods/l/NEEventCalendar.svg?style=flat)](https://cocoapods.org/pods/NEEventCalendar)
[![Platform](https://img.shields.io/cocoapods/p/NEEventCalendar.svg?style=flat)](https://cocoapods.org/pods/NEEventCalendar)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

NEEventCalendar is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'NEEventCalendar'
```

## Author

1217493217@qq.com, chang.liu.meme@funpuls.com

## License

NEEventCalendar is available under the MIT license. See the LICENSE file for more info.
